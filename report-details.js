$(document).ready(function() {
//
//$("#btnSearch").click(function(){
//
//})
$( "#ConditionReport" ).submit(function( event ) {
  event.preventDefault();
  SearchEvent();
});
})
function SearchEvent(page=1)
{
 var obj=new Object();
    obj.time=$("#ddlTime").val()
    obj.project=$("#ddlProject").val()
    obj.ctv=$("#ddlCtv").val()
    $.ajax({
        url: '/api/v1/search-rpdetails/'+page,
        data: {
            'obj':JSON.stringify(obj),
        },
        type: 'POST',
        dataType: 'json',
        complete: function (res, status,data) {
         $("#tbbodydetails").html("");
          $(".pagination").html("");
          $(".page-total").html("");
              if(res.responseJSON.status=="ok")
              {
                $("#tbbodydetails").html("");
                 $(".page-total").html('<footer class="blockquote-footer">'+res.responseJSON.total_record+' records in <cite title="info page">'+res.responseJSON.total_page+' pages</cite></footer>');

               lst=res.responseJSON.report
               len=lst.length;
               var html=""
               if (len>0){
               for( var a in lst){
                    lst[a]=cleanObject(lst[a])
//                alert(lst[a].uuid)
                    console.log(GetFormattedDate(lst[a].updated_at))
                    html="";
                    var ignoredhtml='';
                    if(lst[a].approver_ignored)
                    {
                        ignoredhtml=' style="background:#ced4da"';
                    }

                    html+='<tr'+ignoredhtml+'><td><audio style="width:100px" controls id="'+lst[a].uuid+'""><source src="/media/'+lst[a].link_splited_audio__audio_file+'" type="audio/mpeg"> </td><td> <b>'+lst[a].project__name+'</b> </td>';
                    if(lst[a].choose_text1==true)
                    {
                    html+=' <td> <span class="badge badge-success text-normal">'+lst[a].text1+'</span> <br><footer class="blockquote-footer"><span class="text-purple">'+lst[a].text1_by+'</span>  <cite title="Source Title">'+GetFormattedDate(lst[a].text1_at)+'</cite></footer></td>';
                    }
                    else {
                    html+=' <td> <span class="badge badge-warning text-normal">'+lst[a].text1+'</span> <br><footer class="blockquote-footer"><span class="text-purple">'+lst[a].text1_by+'</span>  <cite title="Source Title">'+GetFormattedDate(lst[a].text1_at)+'</cite></footer></td>';
//                    html+=' <td> <div class="alert alert-primary" role="alert">'+lst[a].text1+'</div> <br><span class="text-purple">'+lst[a].text1_by+'</span> </td>';
                    }
                    if(lst[a].choose_text2==true)
                    {
                    html+=' <td> <span class="badge badge-success text-normal">'+lst[a].text2+'</span> <br><footer class="blockquote-footer"><span class="text-purple">'+lst[a].text2_by+'</span>  <cite title="Source Title">'+GetFormattedDate(lst[a].text2_at)+'</cite></footer></td>';
                    }
                    else {
                     html+=' <td><span class="badge badge-warning text-normal">'+lst[a].text2+'</span> <br><footer class="blockquote-footer"><span class="text-purple">'+lst[a].text2_by+'</span>  <cite title="Source Title">'+GetFormattedDate(lst[a].text2_at)+'</cite></footer></td>';
                    }
//                    html+='<td> '+lst[a].text2+' </td> ';

                    html+='<td> '+lst[a].text_approver+' </td> <td> '+lst[a].reject_type__value+' </td> ';
                    if(lst[a].text_approved)
                    {
                    html+='<td><span class="badge badge-dot mr-4"> <i class="bg-success"></i><span class="status">Đã phê duyệt</span></span><span class="badge badge-info">'+lst[a].text_approved_by+'</span></td> </tr>'
                    }
                    else if(lst[a].ignored)
                    {
                    html+='<td><span class="badge badge-dot mr-4"> <i class="bg-warning"></i><span class="status">Bỏ qua</span></span> <span class="badge badge-info">'+lst[a].ignored_by+'</span></td> </tr>'
                    }
                    else if(lst[a].approver_ignored)
                    {
                    html+='<td><span class="badge badge-dot mr-4"> <i class="bg-danger"></i><span class="status">Từ chối</span></span> <span class="badge badge-info">'+lst[a].approver_ignored_by+'</span></td> </tr>'
                    }
                    else
                    {
                    html+='<td><span class="badge badge-dot mr-4"> <i class="bg-info"></i><span class="status">Đang tiến hành</span></span></td> </tr>'
                    }
                    $("#tbbodydetails").append(html);
               }

                if(res.responseJSON.hasPrev==true){
                $(".pagination").append('<li class="page-item"><a class="page-link" onclick="SearchEvent('+(parseInt(res.responseJSON.current_page) -1)+')">'+(parseInt(res.responseJSON.current_page) -1)+'</a></li>');
                }
                $(".pagination").append('<li class="page-item active"><a class="page-link" >'+(parseInt(res.responseJSON.current_page))+'</a></li>');
                if(res.responseJSON.hasNext==true){
                $(".pagination").append('<li class="page-item"><a class="page-link" onclick="SearchEvent('+(parseInt(res.responseJSON.current_page) +1)+')">'+(parseInt(res.responseJSON.current_page) +1)+'</a></li>');
                }
               }
              }
//            alert(res);
        }
    });
}

function GetFormattedDate(time) {
    if(time=="") return ""
    var todayTime = new Date(time);
    var month = todayTime.getMonth() + 1;
    var day = todayTime.getDate();
    var year = todayTime.getYear();
    var sec = (todayTime.getSeconds());
    var min = (todayTime.getMinutes());
    var hour = (todayTime.getHours());
    return addZero(hour) + ":" + addZero(min) + ":" + addZero(sec)+" " +addZero(day) + "/" + addZero(month) + "/" + addZero(year);
}
function addZero(num){

if(num<10)
{
return "0"+num;
}else {
return num;
}
}
